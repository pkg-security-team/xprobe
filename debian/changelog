xprobe (0.3-6) unstable; urgency=medium

  * Team upload
  * Add patch to fix missing <ctime> import (Closes: #1016241)
  * Refresh current patches to fix unexpected EOF
  * Update standards version, no changes
  * Fix cross.patch to pass CPPFLAGS which were being missed, causing
    the blhc CI job to fail

 -- Neil Williams <codehelp@debian.org>  Mon, 08 Aug 2022 10:36:41 +0100

xprobe (0.3-5) unstable; urgency=medium

  [ Samuel Henrique ]
  * Team upload.
  * Add salsa-ci.yml.
  * Configure git-buildpackage.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set debhelper-compat version in Build-Depends.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * Bump DH to 13.
  * Bump Standards-Version to 4.5.1.
  * Add Rules-Requires-Root: no.
  * Update watch file.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Mon, 01 Feb 2021 18:12:37 +0000

xprobe (0.3-4) unstable; urgency=medium

  * Team upload.
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
  * Add patch to fix cross-building.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #906298)
  * Bump Standards-Version to 4.2.1
  * Bump debhelper compat level to 11
  * Change priority to optional (instead of deprecated extra)
  * Drop the "--with autoreconf" which is implied by debhelper 11
  * Run autoreconf in multiple directories (libs-external/USI++/src too).

 -- Raphaël Hertzog <hertzog@debian.org>  Sun, 02 Sep 2018 12:16:14 +0200

xprobe (0.3-3) unstable; urgency=medium

  * Add a patch to make the build reproducible (Closes: #827572).
    Thanks to Reiner Herrmann
  * Upgrade Standards-version to 3.9.8 (no change needed)

 -- Sophie Brun <sophie@freexian.com>  Mon, 20 Jun 2016 15:33:26 +0200

xprobe (0.3-2) unstable; urgency=medium

  * Take over the package in the pkg-security team with the permission of
    Richard Atterer.
  * Use debhelper 9 (Closes: #817736)
  * debian/control: remove non-working link to homepage project in Description
    (Closes: #467256)
  * Use dh-autoreconf (Closes: #536262, #769286, #756384)
  * Use format 3.0 (quilt): update old patches
  * Add a patch for the manpage
  * Add a debian/watch
  * debian/control: add misc depends

 -- Sophie Brun <sophie@freexian.com>  Mon, 14 Mar 2016 16:09:08 +0100

xprobe (0.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC 4.3 (Closes: #417779).

 -- Luk Claes <luk@debian.org>  Sat, 15 Mar 2008 21:37:25 +0000

xprobe (0.3-1) unstable; urgency=low

  * New upstream version

 -- Richard Atterer <atterer@debian.org>  Wed, 17 Aug 2005 13:43:07 +0200

xprobe (0.2.2-1) unstable; urgency=low

  * New upstream version

 -- Richard Atterer <atterer@debian.org>  Tue, 22 Feb 2005 22:54:24 +0100

xprobe (0.2.1-3) unstable; urgency=low

  * Also re-libtoolized libs-external/USI++/src

 -- Richard Atterer <atterer@debian.org>  Sun, 26 Dec 2004 13:22:45 +0100

xprobe (0.2.1-2) unstable; urgency=low

  * Re-libtoolized to fix build problem on mipsel
    (*sigh*, upstream keeps forgetting about this.)

 -- Richard Atterer <atterer@debian.org>  Fri, 24 Dec 2004 14:07:03 +0100

xprobe (0.2.1-1) unstable; urgency=low

  * New upstream version

 -- Richard Atterer <atterer@debian.org>  Thu, 23 Dec 2004 19:38:12 +0100

xprobe (0.2rc1-2) unstable; urgency=medium

  * Change compiler flags to compile without optimization
    Closes: #264866: FTBFS: g++ internal compiler error: Segmentation
    fault

 -- Richard Atterer <atterer@debian.org>  Sat, 21 Aug 2004 19:53:07 +0200

xprobe (0.2rc1-1) unstable; urgency=low

  * New upstream version, re-libtoolized.

 -- Richard Atterer <atterer@debian.org>  Sat,  9 Aug 2003 00:38:26 +0200

xprobe (0.1-5) unstable; urgency=low

  * Re-libtoolized to bring config.guess etc up to date, closes: #191128
  * Eliminated use of multi-line strings, which cause build failure
    with gcc 3.3, closes: #195560

 -- Richard Atterer <atterer@debian.org>  Sun,  1 Jun 2003 16:39:52 +0200

xprobe (0.1-4) unstable; urgency=low

  * Fix for build error: libs-external/USI++ attempted to link
    non-fPIC object files into a .so, which failed on some arches.
    Since the .so is not used by xprobe, just prevent the attempt to
    create it.

 -- Richard Atterer <atterer@debian.org>  Wed,  7 May 2003 12:20:20 +0200

xprobe (0.1-3) unstable; urgency=low

  * The upstream Makefiles use "clear" to clear the screen. On some
    buildds, this fails because TERM is not set up. Prevent "clear"
    from being called.

 -- Richard Atterer <atterer@debian.org>  Tue,  6 May 2003 21:38:30 +0200

xprobe (0.1-2) unstable; urgency=low

  * Re-libtoolized to fix HPPA build failure, closes: #191128

 -- Richard Atterer <atterer@debian.org>  Tue, 29 Apr 2003 00:44:06 +0200

xprobe (0.1-1) unstable; urgency=low

  * New upstream version. Now called "xprobe2" by upstream, keeping
    "xprobe" package name out of pure lazyness. :-P
    Upstream version is actually "0.1final" - changes from "0.1" make
    it DFSG-free ("nonprofit/educational only" removed) and fix minor
    bugs.
  * Incorporates NMU fixes (compiled against libpcap 0.7), closes: #156203
  * Improved OS detection no longer misreports Windows ME, closes: #121356

 -- Richard Atterer <atterer@debian.org>  Mon,  7 Apr 2003 16:32:40 +0200

xprobe (0.0.2-1) unstable; urgency=low

  * New upstream release
  * Now ought to detect Windows ME correctly, closes: #110989
  * Prevent compiler warnings: uint32_t not found by configure, but
    defined in indirectly included /usr/include/stdint.h
  * Moved X_v1.0.pdf from .orig.tar.gz to Debian .diff.gz
  * Manpage now uses "xprobe" as command name, not "x", closes: #112802

 -- Richard Atterer <atterer@debian.org>  Sat, 27 Oct 2001 10:52:25 +0200

xprobe (0.0.1p1-1) unstable; urgency=low

  * Initial Release. Finishes the ITP process, closes: #108724
  * Added X_v1.0.pdf to upstream sources (.orig.tar.gz) even though it
    is not actually distributed with the xprobe source.
  * Renamed binary from "x" to "xprobe" (upstream will do this in v0.0.2)

 -- Richard Atterer <atterer@debian.org>  Wed, 15 Aug 2001 11:00:04 +0200
